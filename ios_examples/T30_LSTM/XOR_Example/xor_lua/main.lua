torch.setdefaulttensortype('torch.FloatTensor')
model = ""
stats = {}

function loadNeuralNetwork(path)
    print (path)
    print ("Loaded Neural Network -- Success")
    model = torch.load(path)

    print ("Model Architecture --\n")
    print (model)
    print ("---------------------\n")
end

function loadInputStats(path)
    print (path)
    print ("Loaded input stats -- Success")
    model = torch.load(path)

    print ("Input stats --\n")
    print (stats)
    print ("---------------------\n")
end


function classifyExample(tensorInput)
    v = model:forward(tensorInput)
	print(v[1])
	return v[1]
end